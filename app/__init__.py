if __name__ == '__main__':
    print('FONDA LA SABROSONA')
    print("------------------\n")
   
    cliente = input("Cliente: ")

    while True:
        print('\nMENU')
        print('1. Mondongo\t$1.50')
        print('2. Lengua\t$0.75')
        print('3. Pata\t\t$2.00')
        try:
            opcion = int(input("Opcion: "))
        except:
            opcion = -1

        if opcion == 1:
            precio = 1.5
            comida = "mondongo"
            break
        elif opcion == 2:
            precio = 0.75
            comida = "lengua"
            break
        elif opcion == 3:
            precio = 2.0
            comida = "pata"
            break
        else:
            print("Opcion invalida")

    total = precio * 1.07

    print(cliente + ", debe pagar $" + str(total))

    pago = input("Pago? (S/N)")
    if pago.upper() == "S":
        print("Toma tu", comida +  ",", cliente)
    else:
        print('Pedido cancelado!!!')